#!/usr/bin/env node
const fs = require('fs');

const master = require('../package.json');
const app = require('../app/package.json');

app.version = master.version;

const data = JSON.stringify(app, null, '\t');

fs.writeFileSync('app/package.json', data);
