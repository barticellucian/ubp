const { ipcRenderer } = require('electron');
const TabsGeneral = require("./js/TabGeneral");
const TabKeys = require("./js/TabKeys");
const TabFTP = require("./js/TabFTP");
const TabLogs = require("./js/TabLogs");

class MainScreen{
	constructor()
	{
		this.details = {"general":{}, "ftp":[], "keys":[]};
		this.TabGeneral = new TabsGeneral();
		this.TabKeys = new TabKeys();
		this.TabFTP = new TabFTP();
		this.TabLogs = new TabLogs();
		this.addListeners();
	}
	addListeners()
	{
		document.querySelector(".mdl-tabs__tab[href='#keys']").addEventListener("click", (e)=>{
			this.TabKeys.checkKeys();	
		});
		document.querySelector(".mdl-tabs__tab[href='#log']").addEventListener("click", (e)=>{
			this.TabLogs.checkLogs();
		});
		ipcRenderer.on("getData", (event, data)=>{
			this.details = data;
			if(!data.ftp) this.details["ftp"] = {};
			if(!data.keys) this.details["keys"] = [];
		})
	}
}

document.addEventListener("DOMContentLoaded", ()=>{
	let main = new MainScreen();
});