const { ipcRenderer } = require('electron');

class Tab{
	constructor()
	{
		this.details = {"general":{}, "ftp":[], "keys":[]};
		this.logs = [];
		this.keys = [];
		this.pdfs = [];
		this.shared = [];
		this.vue = null;
		this.initVue();
		this.addListeners();	
		
	}
	initVue()
	{
		// console.log("initVue");
	}
	addListeners()
	{
		//messaging
		ipcRenderer.on("updateClient", (event, data)=>{
			Object.assign(this.details, data);
			this.afterDetailsUpdate();
		})
	}
	afterDetailsUpdate()
	{
		// console.log(this.details);
	}
}

module.exports = Tab;