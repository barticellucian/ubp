const { ipcRenderer } = require('electron');
const Tab = require("./Tab");

class LogsTab extends Tab{
	constructor()
	{
		super();
	}
	initVue()
	{
		this.vue = new Vue({
			el: "#log",
			data: {
				logs: this.logs
			},
			computed: {
				orderedLogs: function(){
					return this.logs.sort((a,b)=>{
						return new Date(b.timestamp) - new Date(a.timestamp);
					})
				}
			},
			methods: {
				copyToClipboard: function(text){
					ipcRenderer.send("copy-to-clipboard", text);
				}
			}
		})
	}
	addListeners()
	{
		super.addListeners();
		ipcRenderer.on("displayLogs", (event, logs)=>{
			this.showLogs(logs);
		});
	}
	checkLogs()
	{
		ipcRenderer.send("get-logs");
	}
	showLogs(newLogs)
	{
		for( let i=0, ilen= this.logs.length; i< ilen; i++){
			this.logs.pop();
		}
		newLogs.forEach((log)=>{
			log.timestamp = this.timeConverter(log.timestamp);
			this.logs.push(log);
		});
	}
	timeConverter(UNIX_timestamp)
	{
		let a = new Date(UNIX_timestamp),
			months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
			year = a.getFullYear(),
			month = months[a.getMonth()],
			date = parseInt(a.getDate()) < 10 ? "0" + a.getDate() : a.getDate(),
			hour = parseInt(a.getHours()) < 10 ? "0" + a.getHours() : a.getHours(),
			min = parseInt(a.getMinutes()) < 10 ? "0" + a.getMinutes() : a.getMinutes(),
			sec = parseInt(a.getSeconds()) < 10 ? "0" + a.getSeconds() : a.getSeconds(),
			time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;
	  return time;
	}
}

module.exports = LogsTab;