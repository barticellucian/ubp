const { ipcRenderer } = require('electron');
const Tab = require("./Tab");


class TabFTP extends Tab{
	constructor()
	{
		super();
		this.ftpSet = {"host":"", "pass":"","port":"","user":"","current":false, "deletable": true};
	}
	initVue()
	{
		this.vue = new Vue({
			el: "#ftp",
			data: {
				ftps: []
			},
			mounted: function(){
				this.getData();
			},
			methods: {
				getData: ()=>{
					ipcRenderer.send("get-details");
				},
				setdata: (e, index)=>{
					this.vue.ftps[index][e.target.name] = e.target.value;
					ipcRenderer.send("save-details", this.vue.ftps, true);
				},
				addFtpSet: ()=>{
					this.vue.ftps.push(this.ftpSet);
					ipcRenderer.send("save-details", this.vue.ftps, true);
				},
				removeSet: (index)=>{
					this.vue.ftps.splice(index, 1);
					console.log(this.vue.ftps);
					ipcRenderer.send("save-details", this.vue.ftps, true);
				},
				toggleCurrent: (index)=>{
					if(this.vue.ftps[index].current === true)
						return;
					this.vue.ftps = this.vue.ftps.map((set, i)=>{
						set.current = i === index ? true : false;
						return set;
					})
					this.vue.ftps.map((set, i)=>{
						if(i !== index){
							set.current = false;	
						}
						return set;
					})
					ipcRenderer.send("save-details", this.vue.ftps, true);
				}
			}
		});
		this.updateMDL();
	}
	afterDetailsUpdate()
	{
		this.showFTPsets(this.vue.ftps);
		this.updateMDL();
	}
	showFTPsets(sets)
	{
		if(sets.length === 0) return;
		for( let i=0, ilen= this.vue.ftps.length; i< ilen; i++){
			this.vue.ftps.pop();
		}

		sets.forEach((set)=>{
			this.vue.ftps.push(set);
		});
		this.updateMDL();
	}
	// }
	addListeners()
	{
		ipcRenderer.on("updateUI", (event, details)=>{
			
			this.showFTPsets(details.ftp);
		})
	}
	updateMDL()
	{
		setTimeout(()=>{
			componentHandler.upgradeDom();
		}, 300);
	}
}

module.exports = TabFTP;