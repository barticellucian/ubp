const { ipcRenderer } = require('electron');
const Tab = require("./Tab");

class TabGeneral extends Tab{
	constructor()
	{
		super();
	}
	initVue()
	{
		this.vue = new Vue({
			el: "#general",
			data: {
				details: this.details.general,
				disabled: {
					swipe: false,
					pinch: false,
					zoom: false
				}
			},
			computed: {
				clientType: function(){
					console.log(this.details.client, typeof this.details.client);
					return typeof this.details.client === "undefined" ? "bydefault" : this.details.client;
				}
			},
			methods: {
				setdata: (e, index)=>{
					this.details.general[e.target.name] = e.target.value;
					let obj = {};
					obj[e.target.name] = e.target.value;
					ipcRenderer.send("save-details", obj);
				},
				choosepath: (e)=>{
					ipcRenderer.send("get-path", "getSourceFolder", this.details.general.path);
				},
				setactions: (e)=>{
					if(!this.details.general.hasOwnProperty("disabled_actions")){
						this.details.general.disabled_actions = [];
					}
					let indexof = this.details.general.disabled_actions.indexOf(e.target.value);
					if(indexof > -1){
						this.vue.disabled[e.target.value] = false;
						this.details.general.disabled_actions.splice(indexof, 1);
					}else{
						this.vue.disabled[e.target.value] = true;
						this.details.general.disabled_actions.push(e.target.value);
					}
					ipcRenderer.send("save-details", {disabled_actions: this.details.general.disabled_actions});
				}
			}
		});
	}
	addListeners()
	{
		super.addListeners();
		ipcRenderer.on("getSourceFolder", (event, path)=>{
			Vue.set(this.vue.details, "src", path);
		})
		// ipcRenderer.on("updateUI", (event, details)=>{
		// 	// Vue.set(this.vue.details, details.general);
		// 	// this.vue.details = details.general;
		// })
	}

	afterDetailsUpdate()
	{
		this.vue.details = this.details.general;
		if(this.details.general.disabled_actions){
			this.details.general.disabled_actions.forEach((action)=>{
				this.vue.disabled[action] = this.details.general.disabled_actions && this.details.general.disabled_actions.indexOf(action) > -1;
			})
		}
	}

}

module.exports = TabGeneral;