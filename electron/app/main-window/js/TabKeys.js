const { ipcRenderer } = require('electron');
const Tab = require("./Tab");

class KeysTab extends Tab{
	constructor()
	{
		super();
	}
	initVue()
	{
		this.vue = new Vue({
			el: "#listKeyMessages",
			data: {
				keys: this.keys,
				pdfs: this.pdfs,
				shared: this.shared,
				building: false,
				deploying: false,
				bt: {
					active: false,
					src: "",
					style: {
						display: 'none'
					}
				}
			},
			methods: {
				showbt: function(e){
					let target = e.target;
					this.bt.src = target.src;
					this.bt.active = !this.btactive;
					this.bt.style.display = 'block';
				},
				hidebt: function(e){
					this.bt.style.display = 'none';
				},
				addAsset: (keyM)=>{
					ipcRenderer.send("add-specific-assets", JSON.stringify(keyM));
				},
				deleteSpecificAsset: (asset, keyM)=>{
					ipcRenderer.send("delete-specific-assets", JSON.stringify(keyM), JSON.stringify(asset));	
				},
				toggleKey: (index)=>{
					let targetCard = document.querySelectorAll("#listKeyMessages .key-card")[index];
					targetCard.classList.toggle("expanded");
				},
				togglePdf: (index)=>{
					let targetCard = document.querySelectorAll("#listKeyMessages .key-card")[this.keys.length + index];
					targetCard.classList.toggle("expanded");
				},
				toggleShared: ()=>{
					let targetCard = document.querySelector("#listKeyMessages .key-card#sharedKM");
					targetCard.classList.toggle("expanded");	
				},
				buildKey: (event, name, type)=>{
					event.target.innerHTML = "loading...";
					ipcRenderer.send("build-key", name, type);
				},
				deployKey: (keyM)=>{
					// this.buttonsText.add = "loading";
					document.querySelector(`.deployKey[data-key='${keyM.file}']`).innerHTML = "loading...";
					ipcRenderer.send("deploy-key", JSON.stringify(keyM));
				},
				buildAll: ()=>{
					this.vue.building = true;
					ipcRenderer.send("irep-build");
				},
				deployAll: ()=>{
					ipcRenderer.send("ftp-deploy", this.details);
				}
			}
		});

		Vue.component("description-input", {
			props: ["descvalue", "file"],
			data: function (){ return {description: this.descvalue}; },
			template: "<input class='descriptionInput' v-model:value='description' v-on:blur='saveDescription'>",
			methods: {
				saveDescription: function(){
					ipcRenderer.send("update-key-data", "description", this.file, this.description);
				}
			}	
		})

		Vue.component("prefix-input", {
			props: ["prefvalue", "file", "type"],
			data: function () {return {prefix: this.prefvalue}; },
			template: "<input class='prefixInput' v-model:value='prefix' v-on:blur='savePrefix'>",
			methods: {
				savePrefix: function(){
					ipcRenderer.send("update-key-data", "prefix", this.file, this.prefix, this.type);
				}
			}
		})
	}
	addListeners()
	{
		super.addListeners();
		ipcRenderer.on("getKeysData", (event, keysData)=>{
			this.vue.building = false;
			this.computeKeys(keysData);
		});
		ipcRenderer.on("computedKey", (event, key)=>{
            document.querySelectorAll(".buildKey").forEach((btn)=>{
            	btn.innerHTML = "Build";
            })
        });
        ipcRenderer.on("deployedKey", (event, key)=>{
            document.querySelectorAll(".deployKey").forEach((btn)=>{
            	btn.innerHTML = "Deploy";
            })
        });
	}
	checkKeys(){
		if(!this.keys.length){
			this.scanForKeys();
		}
	}
	computeKeys({keys=[], pdfs=[], shared=[]})
	{
		if(keys.length === 0 && pdfs.length === 0) return;
		for( let i=0, ilen= this.keys.length; i< ilen; i++){
			this.keys.pop();
		}

		for( let i=0, ilen= this.pdfs.length; i< ilen; i++){
			this.pdfs.pop();
		}
		for( let i=0, ilen= this.shared.length; i< ilen; i++){
			this.shared.pop();
		}


		keys.forEach(key=> this.keys.push(key));
		pdfs.forEach(pdf=> this.pdfs.push(pdf));
		shared.forEach(shared=> this.shared.push(shared));
	}
	computeCommons(newCommons=[])
	{
		if(newCommons.length === 0) return;
		for( let i=0, ilen= this.commons.length; i< ilen; i++){
			this.commons.pop();
		}
		newCommons.forEach((asset)=>{
			this.commons.push(asset);
		});
	}
	scanForKeys()
	{
		if(this.details.general.src && this.details.general.src.length){
			ipcRenderer.send("scan-keys");
		}else{
			ipcRenderer.send("show-message", "Warning!", "You need to add the path for the build folder");
		}
	}
	addCommonAsset()
	{
		ipcRenderer.send("add-common-assets", this.details.general);
	}
}

module.exports = KeysTab;