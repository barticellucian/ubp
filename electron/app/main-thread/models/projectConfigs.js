const fs = require("fs");
const ubpConfigs = require("./configs");

class ProjectConfigs
{
	constructor()
	{
		this.globalConfigs = ubpConfigs.get();
		this.path = "";
	}

	/**
	 * Setter
	 *
	 * @param      {Object}   The data
	 * @param      {boolean}  isFtp   Indicates if ftp
	 */
	set(data, isFtp)
	{
		let existingData = this.get();
		if(isFtp){
			existingData.ftp = data;
		}else{
			existingData.general = Object.assign(existingData.general, data);
		}

		try{
			fs.writeFileSync(`${this.globalConfigs.wf}/${this.path}/${this.globalConfigs.folderName}/${this.globalConfigs.fileName}`, JSON.stringify(existingData), 'utf8');
		}catch(err){
			console.log(err);
		}
		return existingData
	}

	/**
	 * getter
	 *
	 * @return     {Object}  { the project configs }
	 */
	get()
	{
		let dataContent = {};

		const addPath = dataContent => dataContent.general.path = this.path;

		//try to get config file content
		try{
			dataContent = JSON.parse(fs.readFileSync(`${this.globalConfigs.wf}/${this.path}/${this.globalConfigs.folderName}/${this.globalConfigs.fileName}`));
		}catch(err)
		{
			console.log(err)
		}

		return dataContent.general.path ? dataContent : addPath(dataContent);

	}

	/**
	 * Sets the project path.
	 *
	 * @param      {string}  path    The path
	 * @return     {string}  { the path }
	 */
	setPath(path)
	{
		this.path = path;
		return this.path;
	}

	/**
	 * Gets the path.
	 *
	 * @return     {string}  The path.
	 */
	getPath()
	{
		return this.path;
	}

}

module.exports = new ProjectConfigs();