const {app} = require("electron");
const fs = require("fs");
const path = require("path");

class Configs
{
	constructor()
	{

		this.configs = {
			dataFolder : app.getPath("appData"),
			folderName : "UBP",
			fileName : "config.json",
			configFile: "config.json",
			keysFile : "keys.json",
			logsFile : "logs.json",
			distFolder : "UBP-dist",
			keysFolder: "UBP-keys",
			sharedFolder: "shared",
			ctlFolder: "ctlfile",
			ftpDetailsFile: "./db/ftp-thg-sandbox.json",
			wf: ""
		}

		let fileData = this.getDataFromFile();
		this.set(fileData);
	}

	/**
	 * setter
	 *
	 * @param      {object}  The data
	 */
	set(data)
	{
		this.configs = Object.assign(this.configs, data);
	}
	/**
	 * getter
	 *
	 * @return     {Object}  { the configs }
	 */
	get()
	{
		return this.configs;
	}

	/**
	 * Retrives data from the config file
	 *
	 * @return     {json}  The data from file.
	 */
	getDataFromFile()
	{
		let dataContent = {};
		//try to get config file content
		let configFile = path.resolve(this.configs.dataFolder,this.configs.folderName,this.configs.fileName);
		if (!fs.existsSync(configFile)){
			this.setWorkFolder("", true);
			return dataContent;
		}
			

		try{
			dataContent = JSON.parse(fs.readFileSync(configFile));
		}catch(err)
		{
			console.log(err);
		}

		return dataContent;
	}



	/**
	 * Sets the work folder.
	 *
	 * @param      {string}  workFolder  The work folder
	 * @param      {boolean}  initial     Is the initial set up of the work folder?
	 * @return     {function}  set the work folder as property of the class
	 */
	setWorkFolder(workFolder, initial)
	{
		//try to create Ubp Folder
		try{
			fs.mkdirSync(`${this.configs.dataFolder}/${this.configs.folderName}`);
		}catch(err){
			if(err.code !== "EEXIST") return console.log(err);
		}
		let dataContent = {wf: workFolder};
		if(!initial){
			dataContent = this.getDataFromFile();
			Object.assign(dataContent, {wf: workFolder});
		}

		//try to write to Ubp config file
		try{
			fs.writeFileSync(`${this.configs.dataFolder}/${this.configs.folderName}/${this.configs.fileName}`, JSON.stringify(dataContent), 'utf8');
		}catch(err){
			console.log(err);
		}

		return this.set({wf: workFolder});
	}

}

module.exports = new Configs();