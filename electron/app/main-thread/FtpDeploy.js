const electron = require('electron');
const ipc = electron.ipcMain;
const Windows = require('../lib/windows');
const Ftp = require('jsftp');
const fs = require('fs');
const path = require('path');

class FtpDeploy{
	constructor(event, pconf, ubpconf, keyM)
	{	
		this.ftpDetails = pconf.ftp.reduce((acc, cv, ci, arr)=>{
			return cv.current === true ? cv : acc;		
		});
		this.pconf = pconf;
		this.ubpconf = ubpconf;
		this.key_dir = path.resolve(ubpconf.wf, pconf.general.path, ubpconf.keysFolder);
		this.destinationDir = this.ftpDetails.destination || "";
		this.filesToUpload = [];
		this.ftp = null;
		this.irepWin = null;
		this.event = event;
		this.keyToDeploy = null;
		if(keyM){
			this.keyToDeploy = this.computeKey(keyM);
		}
		this.init();
		
	}
	computeKey(keyM)
	{
		const state = {};
		state.separator = this.pconf.general.separator || "_";
		state.prefix = this.pconf.general.prefix || "";
		state.sufix = this.pconf.general.sufix || "";
		state.keyPrefix = keyM.prefix || ""; 
		
		let keyNameArr = this.pconf.general.client === "allergan" ? 
			[state.keyPrefix+keyM.name,state.separator,state.prefix,this.pconf.general.name,state.sufix] : 
			[state.keyPrefix+keyM.name, state.prefix, this.pconf.general.name, state.sufix];
		let kname = keyNameArr
						.filter(d => d) // filter out empty values
						.join(state.separator); // add separator betwee

		return kname;
	}
	async init()
	{
		const {host, user, pass} = this.ftpDetails;
		try{

			this.ftp = new Ftp({host, user, pass, debugMode:true});
			this.ftp.on('jsftp_debug', function(eventType, data) {
			  console.log('DEBUG: ', eventType);
			  console.log(JSON.stringify(data, null, 2));
			});

			this.dirParse(this.key_dir);

			this.irepWin = await Windows.IrepWindow(this.filesToUpload.map((ci, i, arr)=>{
				return ci.name;
			}));

			for(let file of this.filesToUpload){
				this.irepWin.webContents.send("keyToCompute", file.name);
				await this.ftpCwd("/");
				let filePathArr = `${this.destinationDir}/${file.path}`.split("/");
				for(let dir of filePathArr){
					if(dir.length > 0){
						try{
							await this.ftpCwd(dir);
						}catch(err){
							console.log(`Try catch err changeWF ${err}`);
						}
					}
				}
				await this.ftpPut(file);
				this.irepWin.webContents.send("computedKey", file.name);
			}
			this.event.sender.webContents.send("deployedKey", true);
			this.irepWin.close();

		}catch(err){
			console.log(err);
		}
		
	}

	dirParse(dirpath)
	{
		//get all files in directory -- in preparation to upload
		let files = fs.readdirSync(dirpath);
		files.forEach((file, i)=>{
			let fullpath = path.join(dirpath, files[i]);
	        let stat = fs.statSync(fullpath);
	        if(/^\./.test(files[i])){
	        	return false;
	        }
	        if(stat.isDirectory()) {
	        	this.dirParse(fullpath);

	        } else {
	        	if(this.keyToDeploy){
	        		if(new RegExp(`${this.keyToDeploy}`,"g").test(file)){
	        			this.filesToUpload.push({"path": path.relative(this.key_dir, path.dirname(fullpath)), "file":fullpath, "name":files[i]});
	        		}
	        	}else{
	        		this.filesToUpload.push({"path": path.relative(this.key_dir, path.dirname(fullpath)), "file":fullpath, "name":files[i]});
	        	}
	        }
		})
	}
	ftpCwd(folder)
	{
		return new Promise((resolve, reject)=>{
			this.ftp.raw.cwd(folder, (err)=>{
				if(err){
					console.log(`${folder} does not exist. we will create it now.`);
					this.createWD(folder)
						.then(()=>{
							this.ftpCwd(folder)
							.then(()=>{
								resolve(`Changed work folder to: ${folder}`);
							})
						})
						.catch((err)=>{
							reject(err);
						})
					
				}else{
					resolve(`Changed work folder to: ${folder}`);
				}
			})
		})
	}
	createWD(dir)
	{
		console.log(`Creating folder ${dir}`);
		return new Promise((resolve, reject)=>{
			this.ftp.raw.mkd(dir, (err)=>{
				if(err){
					reject(`ftpcwd Error creating new remote folder: ${dir} --> ${err}`);
				}else{
					console.log(`Created folder: ${dir}`);
					resolve(`Created folder: ${dir}`);
				}
			})
		})
	}
	ftpPut(file)
	{
		return new Promise((resolve, reject)=>{
			let relPathFile = file.path.length ? `${this.destinationDir}/${file.path}/${file.name}` : `${this.destinationDir}/${file.name}` ;
			this.ftp.put(file.file, relPathFile, (err)=>{
				if(err){
					reject(`ftput err: ${err}`);
				}else{
					console.log(`Uploaded file: ${relPathFile}`);
					resolve(file.name);
				}
			});
		})
	}
}

module.exports = FtpDeploy;