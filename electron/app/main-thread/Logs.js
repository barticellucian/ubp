const electron = require('electron');
const ipc = electron.ipcMain;
const fs = require("fs");
const path = require("path");
const Base = require("./Base");

class Logs extends Base{
	init()
	{
		this.addListeners();
		
	}
	/**
	 * Adds listeners.
	 *
	 */
	async addListeners()
	{
		try{
			ipc.on("get-logs", async event=> event.sender.send("displayLogs", await this.getLogs()) );
		}catch(err){
			console.log(err);
		}
	}

	/**
	 * Rsolve the file path for the log file and save a new log
	 */
	async new(message, type)
	{
		try{
			this.getProjectConfigs();
			let logFilePath = path.resolve(this.ubpconf.wf, this.pconf.general.path, this.ubpconf.folderName, this.ubpconf.logsFile);
			let log = await new Log(message, type, logFilePath);
			console.log(`LOG [${log.type}]: ${log.message}`);
			return {message, type};
		}catch(err){
			console.log(err);
		}
	}

	/**
	 * Return all the logs
	 *
	 * @return     {array}  The logs.
	 */
	async getLogs()
	{
		this.getProjectConfigs();
		let logs = await this.readLogs();
		logs = logs.split("\r\n").filter(entry => entry.length > 0).map(entry => JSON.parse(entry));
		return logs.length === 0 ? [] : logs;
	}

	/**
	 * Reads logs file.
	 *
	 * @return     {Promise}  { resolving the data from the log file-- the logs }
	 */
	readLogs()
	{
		let logFilePath = path.resolve(this.ubpconf.wf, this.pconf.general.path, this.ubpconf.folderName, this.ubpconf.logsFile);
		return new Promise((resolve, reject)=>{
			fs.readFile(logFilePath, "utf8", (err, data)=>{
				if(err){console.log(err); return reject(err)};
				resolve(data);
			});
		})
	}

	/**
	 * Creates a file.
	 *
	 * @param      {<type>}   projectFolder  The project folder
	 * @return     {Promise}  { description_of_the_return_value }
	 */
	createFile(projectFolder)
	{
		return new Promise((resolve, reject)=>{
			fs.writeFile(`${projectFolder}/${this.ubpconf.logsFile}`, "", (err)=>{
				if(err) {throw err; reject(err);}
				console.log("Logs file created: ", `${projectFolder}/${this.ubpconf.logsFile}`);
				resolve(projectFolder);
			});
		});
	}

	/**
	 * clear the contents of the log file
	 */
	clearFile()
	{
		this.getProjectConfigs();
		let logFilePath = path.resolve(this.ubpconf.wf, this.pconf.general.path, this.ubpconf.folderName, this.ubpconf.logsFile);
		fs.writeFileSync(logFilePath, "", "utf8");
	}
}


class Log{
	constructor(message, type, filePath)
	{
		message = type === "error" ? message.stack : message;
		this.log = {timestamp: Date.now(), message, type};
		return this.writeLogToFile(this.log, filePath);
	}
	writeLogToFile(log, filePath)
	{
		return new Promise((resolve, reject)=>{
			fs.appendFile(filePath, JSON.stringify(log)+"\r\n", (err)=>{
				if(err) {return reject(err);}
				return resolve(log);
			})
		});
	}
}


const LogsSingleton = new Logs();
module.exports = LogsSingleton;