const ss = require("./irep/screenshot.js");
const fs = require("fs");
const path = require("path");
const ncp = require("ncp");
var archiver = require('archiver');
ncp.limit = 16;

class Irep{
	constructor()
	{
		this.addListeners();
		this.keysContent = [];
		this.sharedAssets = [];
	}
	addListeners()
	{
		
		const setKeysInState = ({keys = [], pdfs = [], shared = []})=>{
			try{
				this.keysContent = keys.concat(pdfs);
				this.sharedAssets = shared;
			}catch(err){
				console.log(err);
			}

		}
		try{
			process.send({available: true});
			process.on("message", async (msg)=>{
				if(msg.type === "initial"){
					setKeysInState(msg.keys);
					this.ubpconf = msg.data.ubpconf;
					this.pconf = msg.data.pconf;
					this.irepAll = msg.irepAll;
					this.state = {
						distFolder: "",
						keysFolder: "",
						ctlFolder: "",
						separator: "",
						prefix: "",
						sufix: ""
					}
					this.state = await this.initIrep();
					this.startPacking();
				}
			});
		}catch(err){
			console.log(err);
		}
	}

	async initIrep()
	{
		try {
			let state = {};
			let pconfFolderPath = path.resolve(this.ubpconf.wf, this.pconf.general.path);
			state.distFolder = await this.createFolder(pconfFolderPath, this.ubpconf.distFolder, this.irepAll);
			state.keysFolder = await this.createFolder(pconfFolderPath, this.ubpconf.keysFolder, this.irepAll);
			state.ctlFolder = await this.createFolder(state.keysFolder, this.ubpconf.ctlFolder, this.irepAll);
			state.separator = this.pconf.general.separator || "_";
			state.prefix = this.pconf.general.prefix || "";
			state.sufix = this.pconf.general.sufix || "";

			return state;
		}catch(err){
			console.log(err);
		}
	}

	async startPacking()
	{
		if(this.keysContent.length > 0)
			await this.handleKeyMessages();
		if(this.sharedAssets.length > 0){
			await this.handleShared();
		}

		process.send({irepEnded: true});
	}

	async handleKeyMessages()
	{
		for(let key of this.keysContent){
			let keyPrefix = key.prefix || "";
			let keyNameArr = this.pconf.general.client === "allergan" ? [keyPrefix+key.name,this.state.separator,this.state.prefix,this.pconf.general.name,this.state.sufix] : [keyPrefix+key.name, this.state.prefix, this.pconf.general.name, this.state.sufix];
			let kname = keyNameArr
							.filter(d => d) // filter out empty values
							.join(this.state.separator); // add separator betwee

			process.send({keyStartBuild: kname});
			let keyFolder = await this.createFolder(this.state.distFolder, kname, true);
			if(key.ext === "html"){
				//copy .html file
				let htmlPath = this.pconf.general.type === "gsk" ? `${keyFolder}/index.html` :`${keyFolder}/${kname}.html` ;
				let fileToCopy = path.resolve(this.ubpconf.wf, this.pconf.general.path, this.pconf.general.src, key.file);
				await this.copyFile(fileToCopy, htmlPath);
				await this.injectIrepCode(htmlPath);
				//copy specific assets
				this.getAssets(key.assets, keyFolder);
			}else{
				//is pdf
				let pdfPath = `${keyFolder}/${kname}.pdf` ;
				let fileToCopy = path.resolve(this.ubpconf.wf, this.pconf.general.path, this.pconf.general.src, key.file);
				await this.copyFile(fileToCopy, pdfPath);
			}
			try{
				//take screenshot
				await ss.takeScreenshot(keyFolder, kname, key.ext);
				
				//create ctl file
				await this.createCTL(this.state.ctlFolder, kname, key.description);

				//zip up
				await this.zip(this.state.distFolder, this.state.keysFolder, keyFolder, kname);
			}catch(e){
				console.log(e);
			}
			

			process.send({keyBuild: kname});
		}
	}

	async handleShared()
	{
		try{
			let keyname = "SHARED";
			let keyNameArr = this.pconf.general.client === "allergan" ? 
				[keyname,this.state.separator,this.state.prefix,this.pconf.general.name,this.state.sufix] : 
				[keyname,this.state.prefix, this.pconf.general.name, this.state.sufix];
			let kname = keyNameArr
							.filter(d => d) // filter out empty values
							.join(this.state.separator); // add separator betwee
			let keyFolder = await this.createFolder(this.state.distFolder, kname, true);
			let key = this.sharedAssets[0];

			process.send({keyStartBuild: kname});
			
			for(let asset of key.assets){
				let obj = {rel: `${this.ubpconf.sharedFolder}/${asset}`, name: asset.split("/").pop()};
				await this.getAsset(obj, keyFolder, true);
			}
			
			await this.zip(this.state.distFolder, this.state.keysFolder, keyFolder, kname);
			process.send({keyBuild: kname});
			
		}catch(err){
			console.log(err);
		}
	}


	async getAssets(assets, keyFolder)
	{
		if(assets.auto){
			for(let asset of assets.auto){
				await this.getAsset(asset, keyFolder);
			}
		}
		if(assets.manual){
			for(let asset of assets.manual){
				await this.getAsset(asset, keyFolder);
			}
		}
	}

	async getAsset({rel, name}, keyFolder, shared = false)
	{
		
		let assetSubFoldersArr = path.dirname(rel).split("/");
		if(shared)
			assetSubFoldersArr.shift();
		let folderChain = keyFolder;
		// let assetName = assetSubFoldersArr.pop();
		
		
		//create the needed folders
		for(let subFolder of assetSubFoldersArr){
			await this.createFolder(folderChain, subFolder, false);
			folderChain += `/${subFolder}`;
		}

		try{
			//copy file in the key message folder
			let fileToCopy = path.resolve(this.ubpconf.wf, this.pconf.general.path, this.pconf.general.src, rel);
			await this.copyFile(fileToCopy, `${folderChain}/${name}`);
			fs.chmodSync(`${folderChain}/${name}`, 511);
		}catch(e){
			console.log(e);
		}
	}

	async copyFile(source, target)
	{
		return new Promise((resolve, reject)=>{
			let rd = fs.createReadStream(source);

			rd.on("error", (err)=>{ return reject(err); });
			let wr = fs.createWriteStream(target);
			wr.on("error", (err)=>{ return reject(err); });
			wr.on("close", ()=>{
				resolve(target);
			});
			rd.pipe(wr);

		});
	}

	async getAllKeys()
	{
		let keysFilePath = path.resolve(this.ubpconf.wf, this.pconf.general.path, this.ubpconf.folderName, this.ubpconf.keysFile);
		return new Promise((resolve, reject)=>{
			fs.readFile(keysFilePath, "utf8", (err, data)=>{
				if(err){console.log(err); reject(err)};
				resolve(data);
			});
		})
	}

	async injectIrepCode(htmlPath){
		let keysContent = JSON.parse(await this.getAllKeys());
			keysContent = keysContent.keys.concat(keysContent.pdfs);
		let keyPrefixes = {};
		keysContent.forEach((key)=>{
			keyPrefixes[key.name] = key.prefix;
		})

		return new Promise((resolve, reject)=>{
			let fileContent = fs.readFileSync(`${htmlPath}`).toString();
			
			var code = [
				`window.BUILD_FOR_IREP=true;`,
				`window.BUILD_FOR_IREP_PREFIX="${this.prefix}"`,
				`window.BUILD_FOR_IREP_SUFIX="${this.sufix}"`,
				`window.BUILD_FOR_IREP_SEPARATOR="${this.separator}"`,
				`window.BUILD_FOR_IREP_PRESENTATION_NAME="${this.pconf.general.name}"`,
				`window.BUILD_FOR_IREP_KEY_PREFIXES=${JSON.stringify(keyPrefixes)}`
			];
			fileContent = fileContent.replace("</head>", "\t<script>"+code.join("\n")+"</script>\n</head>");
			
			fs.writeFile(htmlPath, fileContent, (err)=>{
				if(err) {
					console.log(err);
					reject(err);
				}
				console.log(`Injected sript in: ${htmlPath}. \n \n`);
				resolve(`Injected sript in: ${htmlPath}.`);
			})
		})
	}

	createCTL(ctlFolder, kname, description)
	{
		return new Promise((resolve, reject)=>{
			description = description || kname;
			let CTL_MAP = {
				"name" : "Name",
				"description" : "Description_vod__c",
				"order" : "Display_Order_vod__c",
				"product" : "Product_vod__c",
				"active" : "Active_vod__c",
				"disabledActions": "Disable_Actions_vod__c"
			};

			let CTL_ACTIONS = {
				"pinch" : "Pinch_To_Exit_vod",
				"swipe" : "Swipe_vod",
				"zoom" : "Zoom"
			};
			
			let ftpDetails = null;

			this.pconf.ftp.forEach((set)=>{
				if(set.current)
					ftpDetails = set;
			})
			let disabledActions = this.pconf.general.disabled_actions || [];
			disabledActions = disabledActions.map(function(devent){
				return CTL_ACTIONS[devent];
			}).join(";");

			let content= [
				`USER=${ftpDetails.user}`,
				`PASSWORD=${ftpDetails.password}`,
				`FILENAME=${kname}.zip`,
				`${CTL_MAP["name"]}=${kname}`,
				`${CTL_MAP["description"]}=${description}`,
				`${CTL_MAP["disabledActions"]}=${disabledActions}`
			].join("\n");

			fs.writeFile(`${ctlFolder}/${kname}.ctl`, content, function(err) {
			    if(err) {
			        console.log(err);
			        reject(err);
			    }


			    console.log(`CTL: ${ctlFolder}/${kname}.ctl saved`);
			    resolve(`${ctlFolder}/${kname}.ctl saved`);
			});
		})
	}

	createFolder(path, folder, rebuild)
	{
		return new Promise((resolve, reject)=>{
			fs.mkdir(`${path}/${folder}`, (err)=>{
				if(err) {
					if(err.code === "EEXIST"){
						if(rebuild){
							this.rmdir(`${path}/${folder}`);
							this.createFolder(path, folder);
						}
					}else{
						reject(err);
					}
				}
				resolve(`${path}/${folder}`);
			});
		});
	}
	rmdir(dir)
	{
	    let list = fs.readdirSync(dir);
	    for(var i = 0; i < list.length; i++) {
	        var filename = path.join(dir, list[i]);
	        var stat = fs.statSync(filename);

	        if(filename == "." || filename == "..") {
	            // pass these files
	        } else if(stat.isDirectory()) {
	            // rmdir recursively
	            this.rmdir(filename);
	        } else {
	            // rm fiilename
	            fs.unlinkSync(filename);
	        }
	    }
	    fs.rmdirSync(dir);
	};
	zip(distFolder, keysFolder, keyFolder, kname)
	{
		return new Promise((resolve, reject)=>{
			let archive = archiver.create("zip", {});
			let output = fs.createWriteStream(`${keysFolder}/${kname}.zip`);

			output.on('close', function() {
			  console.log(`ZIP: ${keyFolder}.zip ${archive.pointer()} total bytes`);
			  resolve(`${keyFolder}.zip ${archive.pointer()} total bytes`);
			});

			// good practice to catch this error explicitly
			archive.on('error', function(err) {
				reject(err);
			  	throw err;
			});
			archive.pipe(output);
			archive
			  // .directory(keyFolder, "/")
			  .bulk({
					"cwd" : distFolder,
					"expand" : true,
					"src" : [`${kname}/**`]
				})
			  .finalize();
		})
	}

}

const irep = new Irep();
