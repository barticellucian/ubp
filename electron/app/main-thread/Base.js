const ubpConfigs = require("./models/configs");
const projectConfigs = require("./models/projectConfigs");

class Base
{
	constructor()
	{
		this.ubpconf = ubpConfigs.get();
		this.pconf = {};
	}
	/**
	 * get the project relative path to the work folder
	 *
	 * @return     {string}  The project path.
	 */
	getProjectPath()
	{
		return projectConfigs.getPath();
	}

	/**
	 * Sets the project path.
	 *
	 * @param      {string}  path    The path
	 * @return     {object}  { the new pronject config details }
	 */
	setProjectPath(path)
	{
		projectConfigs.setPath(path);
		this.pconf = projectConfigs.get();
		return this.pconf;
	}

	/**
	 * Gets the project configs.
	 */
	getProjectConfigs()
	{
		this.pconf = projectConfigs.get();
		return this.pconf;
	}


	/**
	 * Sets details in the project configs.
	 *
	 * @param      {<type>}  data    The data
	 */
	setProjectConfigs(data, isFtp)
	{
		this.pconf = projectConfigs.set(data, isFtp);
	}

	setWorkFolder(path)
	{
		ubpConfigs.setWorkFolder(path);
	}
}

module.exports = Base;