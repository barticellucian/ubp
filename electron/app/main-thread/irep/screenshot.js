const phantom = require('phantom');
const gm = require('gm');
const fs = require("fs");


module.exports.takeScreenshot = function _takeScreenshot(path, name, ext)
{
	var sitepage = null;
	// var phInstance = null;
	var i = 0;
	var settings = {format: "png", quality:70};
	var output = `${path}/${name}-thumb.${settings.format}`;
	var tmp    = `${path}/${name}-full.${settings.format}`;
	var width = 400;
	var height = 300;

	const shotHTML = (path, name, ext)=>{


		return new Promise((resolve, reject)=>{
			phantom.create([], {
				logLevel: 'warn'//'debug',
			})
		    .then(instance => {
		        phInstance = instance;
		        return instance.createPage();
		    })
		    .then(page => {
		        sitepage = page;
		        let km = `${path}/${name}.${ext}`;
		        
		        return sitepage.open(km);
		    })
		    .then(status => {
		        sitepage.property('viewportSize', {
		        	width : 1024,
		        	height: 768
		        });
		    })
		    .then(something => {
		    	return sitepage.render(tmp, settings);
		    })
		    .then(something => {
		    	return phInstance.exit();
		    })
		    .then(() => {
		    	//install graphicsmagic so this can work	
		    	//brew install graphicsmagick
		    	gm(tmp)
		    		.resize(width, height)
		    		.write(output, function(){

		    		console.log(`Screens done : ${path}/${name}.${ext}`);
		    		resolve();

		    	});
		    })
		    .catch(e => {
		    	console.log(e);
		    	reject(e);
		    });
		})
	}

	const shotPDF = (path, name, ext)=>{
		
		return new Promise((resolve, reject)=>{
		    gm(`${path}/${name}.${ext}[0]`)
		    	.gravity("Center")
	    		.resize(1024,768, "^")
	    		.crop(1024,768,0,0)
	    		.write(tmp, function(err){
	    			if(err) return console.log(err);
	    			gm(tmp)
			    		.resize(width, height)
			    		.write(output, function(){
				    		console.log(`Screens done : ${path}/${name}.${ext}`);
				    		resolve();
			    		});
	    		})
	    	});
	}


	return ext === "html" ? shotHTML(path,name,ext) : shotPDF(path,name,ext);
}

