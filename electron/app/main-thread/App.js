const electron = require('electron');
const app = electron.app;
const Menu = electron.Menu;
const dialog = electron.dialog;
const ipc = electron.ipcMain;
const Package = require('../package.json');
const Windows = require('../lib/windows');

const Main = require("./Main");
const Logs = require("./Logs");
const Keys = require("./Keys");

class AppClass{
	constructor()
	{
		this.infoWindow = null;
		this.manageExceptions();
		this.loadConfig();
		this.checkIfDev();
		this.addDevTools();
		this.makeSingleInstance();
		this.checkSingleInstance();
		this.addListeners();
		app.setName(Package.productName || '');

		Main.init();
		Logs.init();
		Keys.init();
	}
	manageExceptions()
	{
		/// Manage unhandled exceptions as early as possible
		process.on('uncaughtException', (err) => {
			console.error(`Caught unhandled exception: ${err}`);
			dialog.showErrorBox('Caught unhandled exception', err.message || 'Unknown error message');
			app.quit();
		});
	}
	loadConfig()
	{
		// Load build target configuration file
		try {
			let config = require('./config.json');
			Object.assign(Package.config, config);
		} catch (e) {
			console.warn('No config file loaded, using defaults');
		}
	}
	checkIfDev()
	{
		global.isDev = (require('electron-is-dev') || Package.config.debug);
		global.appSettings = Package.config;

		if (global.isDev) {
			console.info('Running in development');
		} else {
			console.info('Running in production');
		}



		return global.isDev;
	}
	addDevTools()
	{
		// Adds debug features like hotkeys for triggering dev tools and reload
		// (disabled in production, unless the menu item is displayed)
		require('electron-debug')({
			enabled: Package.config.debug || global.isDev || false
		});

	}
	makeSingleInstance() 
	{
		/// Function to make this app a single instance app.
		//
		// The main window will be restored and focused instead of a second window
		// opened when a person attempts to launch a second instance.
		//
		// Returns true if the current version of the app should quit instead of
		// launching.
		return app.makeSingleInstance(() => {
			if (Windows.List.length) {
				if (Windows.List[0].isMinimized()) Windows.List[0].restore();
				Windows.List[0].focus();
			}
		});
	}
	checkSingleInstance()
	{
		/// Make sure it is a single instance
		var shouldQuit = this.makeSingleInstance();
		if(shouldQuit) return app.quit();
	}
	addListeners(){
		/// Global Lifecycle Events
		app.on('ready', ()=>{
			Menu.setApplicationMenu(this.createMenu());
			Windows.InitialWindow();
		});

		app.on('window-all-closed', function appWindowAllClosed() {
			// Common in MacOS for apps to have their menu bar active unless explicitly quit (e.g. CMD + Q)
			app.quit();
		});

		app.on('activate', function() {
			// Common in MacOS for apps to re-create intro/primary window when dock icon clicked when there are no others
			if(!Windows.List.length) Windows.AppSwitchWindow();
		});

		app.on('will-quit', () => {});

		//open infoWindow
		ipc.on('open-info-window', () => {
			this.handleOpenInfoWindow();
		});
	}
	handleOpenInfoWindow()
	{
		if(this.infoWindow) return;	
	 	this.infoWindow = Windows.this.InfoWindow();
	 	this.infoWindow.on('closed', () => {
			this.infoWindow = null
		});
	}
	createMenu() 
	{
		return Menu.buildFromTemplate(require('../lib/menu'));
	}
}

const App = new AppClass();
module.exports = App;