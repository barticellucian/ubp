const {dialog, ipcMain:ipc, clipboard} = require('electron');
const glob = require("glob");
const Windows = require('../lib/windows');
const fs = require("fs");
const path = require("path");
const Notify = require("../lib/notify");
const Logs = require("./Logs");
const Keys = require("./Keys");
const cp = require("child_process");
const FtpDeploy = require("./FtpDeploy");

const Base = require("./Base");


class MainClass extends Base{
	init()
	{
		this.addListeners();
	}
	addListeners()
	{
		ipc.on("open-app", (event, projectPath)=>{
			this.handleOpenApp(event, projectPath);
		});
		ipc.on("get-path", (event, cb, defaultPath)=>{
			let dpath = defaultPath ? path.resolve(this.ubpconf.wf, defaultPath) : "/";
			dialog.showOpenDialog({defaultPath: dpath, properties: ['openDirectory']}, (response)=>{
				if(typeof response !== "undefined") {
					let responsePath = response[0];
					if(cb === "setWorkFolder"){
						this.setWorkFolder(responsePath);
						event.sender.send(cb, responsePath);
					}

					if(cb === "getSourceFolder"){
						responsePath = path.relative(`${this.ubpconf.wf}/${this.pconf.general.path}`, response[0]);
						this.handleSaveDetails(event, {src: responsePath})
					}
				}
			});
		});

		ipc.on("scan-for-projects", (event)=>{
			let wf = this.ubpconf.wf || null;
			let projects = wf ? this.scan(wf, true) : [];
			event.sender.send("availableProjects", projects);
		});
		ipc.on("save-details", (e, details, isFtp = false)=>{
			this.handleSaveDetails(e, details, isFtp);
		});
		ipc.on("get-details", e=>{
			e.sender.send("updateUI", this.getProjectConfigs());
		})
		ipc.on("show-message", (event, title, message)=>{
			new Notify(title, message);
		});
		ipc.on("irep-build", (event)=>{
			this.irepBuild(event);
		});
		ipc.on("ftp-deploy", (event, data)=>{
			this.deploy(event, data);
		});
		ipc.on("build-key", (e, name, type)=>{
			this.irepBuildKey(e, name, type);	
		})

		ipc.on("deploy-key", (e, keyM)=>{
			this.deployKey(e, keyM);	
		})

		ipc.on("get-work-folder", e=>{
			let configData = this.ubpconf;
			let wf = this.ubpconf.wf || null;

			e.sender.send("workFolder",wf);
		})

		ipc.on("copy-to-clipboard", (e, text)=>{
			clipboard.writeText(text);
			new Notify("Copy", "Text has been successfully copied to clipboard");
		})


	}
	
	
	/**
	 * handles the opening of a new project or of an existing one
	 *
	 * @param      {object}  event        The event
	 * @param      {string}  projectPath  The project path
	 * @return     void
	 */
	

	handleOpenApp(event, projectPath)
	{	
		const sendDataToClient = ()=>{
			Logs.new(`Opening project: ${this.pconf.general.path}`, "info");
			let win = Windows.MainWindow();
			win.webContents.on('did-finish-load', () => {
				win.webContents.send("updateClient", this.pconf);
		    });
		}

		if(projectPath){
			let projectData = this.setProjectPath(projectPath);
			Logs.clearFile();
			return sendDataToClient();
		}


		dialog.showOpenDialog({properties: ['openDirectory']}, (response)=>{

			if(typeof response !== "undefined") {
				let isInited = this.scan(response[0]).length > 0;
				if(!isInited){
					let workFolder = this.ubpconf.wf;
					let ppath = path.relative(workFolder, response[0]);

					this.createUBPFolder(ppath)
						.then(projectFolder=>this.createUBPProjectConfig(projectFolder,ppath))
						.then(projectFolder=>Keys.createFile(projectFolder))
						.then(projectFolder=>Logs.createFile(projectFolder))	
						.then(()=>{
							let projectData = this.setProjectPath(ppath);
							return sendDataToClient();
						})
						.catch((err)=>{
							Logs.new(err, "error");
						})
				}else{
					return sendDataToClient();
				}
			}
		});

	}

	/**
	 * Search for folder that have the ubp config files in them
	 *
	 * @param      {string}  path        The path
	 * @param      {boolean}  workFolder  is this path the work folder?
	 * @return     {Array}   an array of projects found at this path
	 */

	scan(path, workFolder)
	{

		let projArr = [];

		let reg = workFolder ? `*/${this.ubpconf.folderName}/${this.ubpconf.configFile}` : `/${this.ubpconf.folderName}/${this.ubpconf.configFile}`;
		let results = glob.sync(reg, {cwd: path});

		if(results.length > 0){
			return results.map((file)=>{
				let retObj = {};
				let config = JSON.parse(fs.readFileSync(`${path}/${file}`));
				retObj["name"] = config.general.name;
				retObj["path"] = config.general.path;
				return retObj;
			});
		}
		return projArr;
	}

	/**
	 * Save the details inputed by users
	 *
	 * @param      {object}  event   The event
	 */

	handleSaveDetails(event, details, isFtp = false)
	{
		
			this.setProjectConfigs(details, isFtp);
			event.sender.send("updateClient", this.pconf);
			Logs.new("Your details have been successfully saved", "info");
			new Notify("Project Saved", "Your details have been successfully saved");
		
	}

	
	/**
	 * Creates an folder for the selected project, if there is none.
	 *
	 * @param      {string}   projectPath  The project path
	 * @return     {Promise}  { description_of_the_return_value }
	 */
	createUBPFolder(projectPath)
	{
		let workFolder = this.ubpconf.wf;
		let projectFolder = `${workFolder}/${projectPath}/${this.ubpconf.folderName}`;
		return new Promise((resolve, reject)=>{
			fs.mkdir(projectFolder, (err)=>{
				if(err) { return reject(err);}
				Logs.new(`UBP folder created: ${projectFolder}`, "info");
				resolve(projectFolder);
			});	
		});
	}

	/**
	 * Creates an ubp project configuration.
	 *
	 * @param      {string}   ppath   The ppath
	 * @return     {Promise}  { description_of_the_return_value }
	 */

	createUBPProjectConfig(projectFolder,ppath)
	{
		// return new Promise((resolve, reject)=>{
			
		// })
		
		let configFilePath = this.ubpconf.configFile;

		return new Promise((resolve, reject)=>{
			let projObj = {};
			projObj["path"] = ppath;
			projObj["name"] = ppath.split("/").pop();

			//get default ftp details 
			let ftpDetailsPath = path.resolve(__dirname, this.ubpconf.ftpDetailsFile);
			let ftpDetails = JSON.parse(fs.readFileSync(ftpDetailsPath).toString());
			
			fs.writeFile(`${projectFolder}/${configFilePath}`, JSON.stringify({"general": projObj, "ftp": [ftpDetails]}), (err)=>{
				if(err) { return reject(err);}
				Logs.new(`Config file created: ${projectFolder}/${configFilePath}`, "info");
				resolve(projectFolder);
			});	
		});
	}
	
	deploy(event)
	{
		new FtpDeploy(event, this.pconf, this.ubpconf);
	}
	async deployKey(event, keyM)
	{
		try{
			let key = JSON.parse(keyM);
			new FtpDeploy(event, this.pconf, this.ubpconf, key);
		}catch(err){
			Logs.new(err, "error");
		}
	}

	async irepBuild(event)
	{
		let savedContent = await Keys.scanKeys();
		savedContent.shared = await Keys.scanShared();
		this.forkIrep(event, savedContent, true);
	}

	async irepBuildKey(event, keyName, type)
	{
		try{
			let keys = {}, key = [];
			switch(type){
				case "keys":
				case "pdfs":
					let scannedContent = await Keys.scanKeys();
					scannedContent = scannedContent[type];
					key = scannedContent.filter(({name}) => name === keyName);
					break;
				case "shared":
					key = await Keys.scanShared();
					break;
			}

			keys[type] = key;
			this.forkIrep(event, keys, false);	
		}catch(err){
			Logs.new(err, "error");
		}
		

		
	}

	 forkIrep(event, keyMessages, irepAll){
		let irepWin = null;
		const irep = cp.fork(`${__dirname}/IrepLoader.js`);

		irep.on("message", async (msg)=>{
			if(msg.available){
				irepWin = await Windows.IrepWindow(keyMessages);
				irep.send({
					type: "initial", 
					keys: keyMessages, 
					data: {
						ubpconf: this.ubpconf, 
						pconf: this.pconf
					}, 
					irepAll: irepAll
				});
			}
			if(msg.keyStartBuild){
				irepWin.webContents.send("keyToCompute", msg.keyStartBuild);
			}
			if(msg.keyBuild){
				irepWin.webContents.send("computedKey", msg.keyBuild);
				event.sender.webContents.send("computedKey", msg.keyBuild);
			}
			if(msg.irepEnded){
				irepWin.close();
				Keys.handleScanKeys(event);
				//scan the keys
			}
		});
	}

	
}

const Main = new MainClass();
module.exports = Main;