const electron = require('electron');
const dialog = electron.dialog;
const ipc = electron.ipcMain;
const glob = require("glob");
const Notify = require("../lib/notify");
const fs = require("fs");
const path = require("path");
const Base = require("./Base");
const _ = require('lodash');
const Logs = require("./Logs");

class KeysClass extends Base{
	init()
	{
		this.addListeners();
	}
	addListeners()
	{
		ipc.on("scan-keys", (event, data)=>{
			this.getProjectConfigs();
			this.handleScanKeys(event);
		});
		ipc.on("add-specific-assets", (e, keyM)=>{
			this.handleAddSpecificAssets(e, keyM);
		});
		ipc.on("delete-specific-assets", (e, keyM, asset)=>{
			this.handleDeleteSpecificAssets(e, keyM, asset);
		});
		ipc.on("update-key-data", (e, detail, keyFile, value, type)=>{
			this.handleUpdateKeyData(detail, keyFile, value, type);
		})
	}
	
	/**
	 * Creates the keys.json file initially
	 *
	 * @param      {string}   projectFolder  The project folder
	 * @return     {Promise}  { promise resolving after the file is created }
	 */

	createFile(projectFolder)
	{
		return new Promise((resolve, reject)=>{
			fs.writeFile(`${projectFolder}/${this.ubpconf.keysFile}`, "", (err)=>{
				if(err) { return reject(err);}
				Logs.new(`Keys file created: ${projectFolder}/${this.ubpconf.keysFile}`, "info");
				resolve(projectFolder);
			});
		});
	}
	
	/**
	 * Self explanatory
	 *
	 * @param      {event}   event   The event
	 * @return     {Promise}  { resolving after the key scanning is finished }
	 */
	async handleScanKeys(event)
	{
		try{
			let keys = await this.scanKeys();
			let sharedAssets = await this.scanShared();
			keys.shared = sharedAssets;
			let keysAndShared = await this.writeToFile(keys);
			new Notify("Keys Scanned", "Scanning for key messages has finished");
			event.sender.send("getKeysData", keysAndShared);
			return keysAndShared;
		}catch(err){
			Logs.new(err, "error");
		}
		
	}

	/**
	 * Scans keys for assets and thumbnails.
	 *
	 * @return     {Promise}  { resolving after the scan ended }
	 */

	async scanKeys()
	{
		try{

			let foundKeys = [];
			switch(this.pconf.general.type){
				case "gsk":
					foundKeys = await this.searchGSKKeyMessages();
					break;
				default:
					foundKeys = await this.searchKeyMessages();
					break;
			}

			let foundPdfs = await this.searchPDFs();

			let keysWithAssets = new KeyAssets(foundKeys, this.pconf.general, this.ubpconf); //scan for assets, add them in this array
			                                                                                 //
			let updatedContent = await this.updateContent(keysWithAssets, foundPdfs);

			return await this.checkThumbs(updatedContent);
		}catch(err){
			Logs.new(err, "error");
		}
	}



	/**
	 * Adds a new content.
	 *
	 * @param      {Object-> Array}   kmessages  The kmessages
	 * @param      {Object-> Array}   pdfs       The pdfs
	 * @return     {Promise}  { description_of_the_return_value }
	 */
	async updateContent(kmessages, pdfs)
	{

		const areKeysEqual = (key1, key2, propsToCheck) =>{
			let flag = true;
			for(var i=0, ilen = propsToCheck.length; i< ilen; i++){
				if(!_.isEqual(key1[propsToCheck[i]], key2[propsToCheck[i]])){
					flag = false;
				}
			}
			return flag;
		}


		const isItemInArray = (item, array, propsToCheck) =>{
			let flag = false;
			for(var i=0, ilen = array.length; i< ilen; i++){
				if(areKeysEqual(array[i], item, propsToCheck)){
					flag = true
				}
			}
			
			return flag;
		}
		 
		const arayDIff = (arr1, arr2, propsToCheck) => {
			let newArray = [];
			for(var i=0, ilen = arr1.length; i<ilen; i++ ){
				if(!isItemInArray(arr1[i], arr2, propsToCheck)){
					newArray.push(arr1[i]);
				}
			}
			return newArray;
		}

		const deleteItemFromArray = ({file: fileToDelete}, array) => {
			return array.filter(({file}) => file !== fileToDelete );
		}

		const updateItemInArray = (itemToUpdate, array) => {
			if(!isItemInArray(itemToUpdate, array, ["file"])){
				array.push(itemToUpdate);
			}else{
				array = array.map((item, index, array) => itemToUpdate.file === item.file ? itemToUpdate : item );
			}

			return array;
		}

		const updateArray = (arrayToUpdate, updatedItems, deletedItems) => {


			let newArray = arrayToUpdate;

			

			deletedItems.forEach(itemToDelete => {
				newArray = deleteItemFromArray(itemToDelete, newArray);
			})


			updatedItems.forEach(itemToUpdate => {
				newArray = updateItemInArray(itemToUpdate, newArray);
			})

			return newArray;
		}

		try{
			var fileContents = {keys: [], pdfs: [], shared: []};

			try{
				fileContents = JSON.parse(await this.readKeysFile());
			}catch(e){
				Logs.new("keys file is probably just been created.skip reading for now.", "info");
			}
			let newOrModifiedKeys = arayDIff(kmessages, fileContents.keys, ["file"]),
				deletedKeys = arayDIff(fileContents.keys, kmessages, ["file"]),
				newOrModifiedPdfs = arayDIff(pdfs, fileContents.pdfs, ["file"]),
				deletedPdfs = arayDIff(fileContents.pdfs, pdfs, ["file"]);

			let newKeyArray = updateArray(fileContents.keys, newOrModifiedKeys, deletedKeys),
				newPdfsArray = updateArray(fileContents.pdfs, newOrModifiedPdfs, deletedPdfs);
			

			return {keys: newKeyArray, pdfs: newPdfsArray};	
			
		}catch(err){
			Logs.new(err, "error");
		}
		
		
	}



	/**
	 * Check if key message has thumb. Removes the thumb is there's no entry in the keys.json
	 *
	 * @param      {Object}   the key message
	 * @return     {Promise}  { resolving after the check has finished }
	 */
	async checkThumbs(keysObj)
	{


		const performCheck = key => {
			let separator = this.pconf.general.separator || "_",
				prefix = this.pconf.general.prefix || "",
				sufix = this.pconf.general.sufix || "",
				keyPrefix = key.prefix || "";
			let keyNameArr = this.pconf.general.client === "allergan" ? [keyPrefix+key.name,separator,prefix,this.pconf.general.name,sufix] : [keyPrefix+key.name, prefix, this.pconf.general.name, sufix];
			let keyName = keyNameArr
							.filter(d => d) // filter out empty values
							.join(separator); // add separator betwee

			let thumbPath = path.resolve(this.ubpconf.wf, this.pconf.general.path, this.ubpconf.distFolder, keyName);
			let thumb = `${thumbPath}/${keyName}-full.png`;

			///the thumb path will be absolute. 
			//it will change at every scan, so it should not be a problem

			if (fs.existsSync(thumb)) {
				key.thumb = thumb;
			}else{
				if(key.hasOwnProperty("thumb")){
					delete key.thumb;
				}
			}
			return key;
		}


		return new Promise((resolve, reject)=>{
			keysObj.keys.map(key => performCheck(key));
			keysObj.pdfs.map(pdf => performCheck(pdf));

			resolve(keysObj);
		});
	}


	// searchGSKKeyMessages()
	// {
	// 	return new Promise((resolve, reject)=>{
	// 		// console.log(this.pconf.general.src);

	// 		glob("**/*.html", {cwd: this.pconf.general.src}, (err, files)=>{
	// 			if(err){
	// 				reject(err);
	// 				throw(err);
	// 			}
	// 			let forgedKeys = files.map((key)=>{
	// 				let keyName = key.match(/\/+(\w.+)+(?=\/index)/g)[0].replace("/", "");
	// 				let minKeyName = keyName === "shared" ? keyName : keyName.split("_").pop();
	// 				// let keyFolder = key.split("/").shift().split("_").pop();
	// 				let obj = {};
	// 				obj.file = path.resolve(this.pconf.general.src,key);
	// 				// obj.folder = keyFolder;
	// 				obj.assets = {};
	// 				obj.name = minKeyName;
	// 				return obj;
	// 			});
	// 			// console.log(forgedKeys);
	// 			resolve(forgedKeys);
	// 		})
	// 	})
	// }
	// 
	
	/**
	 * Scan the source/build folder for .pdfs files
	 *
	 * @return     {Promise}  { Promise that resolves the list of pdf files }
	 */
	searchPDFs()
	{
		let projectAbsPath = path.resolve(this.ubpconf.wf, this.pconf.general.path);
		let sourceFilesAbsPath = path.resolve(projectAbsPath, this.pconf.general.src);

		return new Promise((resolve, reject)=>{
			glob("*.pdf", {cwd: sourceFilesAbsPath}, (err, files)=>{
				if(err){
					return reject(err);
				}

				let forgedKeys = files.map((key)=>{
					let obj = {};
					obj.file = key;
					let keyArr = key.split(".");
					let extension = keyArr.pop();
					obj.ext = extension;
					obj.name = keyArr.join();
					return obj;
				});

				resolve(forgedKeys);
			})
		})	
	}

	/**
	 * Scans the folder for shared files in the "shared" folder
	 *
	 * @return     {Array}  { containing the shared object }
	 */
	async scanShared()
	{
		let sourceFilesAbsPath = path.resolve(this.ubpconf.wf, this.pconf.general.path, this.pconf.general.src);
		let sharedFolder = path.resolve(sourceFilesAbsPath, this.ubpconf.sharedFolder);
		if (!fs.existsSync(sharedFolder))
			return Logs.new("no shared folder", "info"); 

		let assets = await this.searchSharedAssets(sharedFolder);

		let sharedObj = {
			name: this.ubpconf.sharedFolder,
			assets: assets
		}

		return [sharedObj];
	}

	/**
	 * Search files in the "shared folder"
	 *
	 * @param      {String}   sharedFolder  Path to the folder
	 * @return     {Promise}  { resolving the files in the folder }
	 */
	searchSharedAssets(sharedFolder)
	{
		return new Promise((resolve, reject)=>{
			glob("**/*", {cwd: sharedFolder}, (err, files)=>{
				if(err) {
					return reject(err);
				}
				files = files.filter(file=> fs.lstatSync(path.resolve(sharedFolder, file)).isFile());
				resolve(files);
			})
		})
	}

	/**
	 * Search .html files in the source/build folder
	 *
	 * @return     {Promise}  { resolving the list of .html files }
	 */
	searchKeyMessages()
	{
		let projectAbsPath = path.resolve(this.ubpconf.wf, this.pconf.general.path);
		let sourceFilesAbsPath = path.resolve(projectAbsPath, this.pconf.general.src);

		return new Promise((resolve, reject)=>{
			glob("*.html", {cwd: sourceFilesAbsPath}, (err, files)=>{
				if(err){
					reject(err);
					throw(err);
				}
				// let separator = this.pconf.general.separator ? this.pconf.general.separator : "_",
				// 	prefix = this.pconf.general.prefix ? (this.pconf.general.prefix + separator) : "",
				// 	sufix = this.pconf.general.sufix ? (separator + this.pconf.general.sufix) : "";

				let forgedKeys = files.map((key)=>{
					let obj = {};
					obj.file = key;
					let keyArr = key.split(".");
					let extension = keyArr.pop();
					obj.ext = extension;
					obj.name = keyArr.join();
					obj.assets = {};
					return obj;
				});

				resolve(forgedKeys);
			})
		})
		
	}
	
	/**
	 * Reads the keys file.
	 *
	 * @return     {Promise}  { resolving the contents of the keys file }
	 */
	readKeysFile()
	{
		let keysFileAbsPath = path.resolve(this.ubpconf.wf, this.pconf.general.path, this.ubpconf.folderName, this.ubpconf.keysFile);

		return new Promise((resolve, reject)=>{
			fs.readFile(keysFileAbsPath, "utf8", (err, data)=>{
				if(err){ return reject(err)};
				resolve(data);
			});
		})
	}
	
	/**
	 * Writes to the keys file.
	 *
	 * @param      {Object}   data    The data
	 * @return     {Promise}  { resolving the data added to the file }
	 */
	writeToFile(data)
	{
		let keysFileAbsPath = path.resolve(this.ubpconf.wf, this.pconf.general.path, this.ubpconf.folderName, this.ubpconf.keysFile);
		return new Promise((resolve, reject)=>{
			fs.writeFile(keysFileAbsPath, JSON.stringify(data), (err)=>{
				if(err){
					reject(err);
					throw(err);
				}
				resolve(data);
			});
		})
	}

	/**
	 *  Updates a details from a specific key message in the keys file
	 *
	 * @param      {string}   detail   The property that needs to be updated
	 * @param      {string}   keyFile  The key message to update
	 * @param      {string}   value    The value
	 * @param      {string}   type of key message     .html/.pdf
	 */
	async handleUpdateKeyData(detail, keyFile, value, type)
	{
		try{
			let fileContents = JSON.parse(await this.readKeysFile());
			let keyType = type === "pdf" ? "pdfs" : "keys";
			let updatedKeys = fileContents;
			updatedKeys[keyType] = fileContents[keyType].map((key)=>{
				if(key.file === keyFile){
					key[detail] = value;
				}
				return key;
			})
			await this.writeToFile(updatedKeys);
			new Notify(`Key ${keyFile} Updated`, `The ${detail} has been updated to "${value}"`);
		}catch(err){
			Logs.new(err, "error");
		}
	}

	/**
	 * handle addition of an asset file
	 *
	 * @param      {object}  event       { we need this so we know who to send the file path back }
	 * @param      {string}  keyM    The key message to add the asset to
	 */
	handleAddSpecificAssets(e, keyM)
	{
		const add_asset = async (assetsToAdd, keyMessage) =>{
			try{
				let fileContents = await this.readKeysFile();
				let updatedContents = this.addAsset(assetsToAdd, keyMessage, fileContents);
				let savedContent = await this.writeToFile(updatedContents);
				e.sender.send("getKeysData", savedContent);
				new Notify("Asset added", "The asset has been added to the key message");
			}catch(err){
				Logs.new(err, "error");
			}
		}


		dialog.showOpenDialog({properties: ['openFile','multiSelections']}, (response)=>{
			if(typeof response == "undefined") return;
			add_asset(response, JSON.parse(keyM));
		})
	}

	/**
	 * { function_description }
	 *
	 * @param      {object}   event           { we need this so we know who to send the file path back }
	 * @param      {string}  keyM    The key message to add the asset to
	 * @param      {string}   asset       The asset to delete
	 * 
	 */
	async handleDeleteSpecificAssets(e, keyMessage, asset){
		try{
			let keyMessageToUpdate = JSON.parse(keyMessage);
			let assetToDelete = JSON.parse(asset);
			let fileContents = await this.readKeysFile();
			let updatedContents = this.deleteAsset(assetToDelete, keyMessageToUpdate, fileContents);
			let savedContent = await this.writeToFile(updatedContents);
			e.sender.send("getKeysData", savedContent);
			new Notify("Item deleted", "The item you have selected has been deleted");
		}catch(err){
			Logs.new(err, "error");
		}
	}

	/**
	 * Check if assets in key message, and tries to add it
	 *
	 * @param      {array}   assetsToAdd   The array of assets to add
	 * @param      {object}   keyMessage    The key message
	 * @param      {object}   fileContents  The file contents before addition
	 * @return     {object}  { the content with the added assets }
	 */
	addAsset(assetsToAdd, keyMessage, fileContents){
		const contentObject = Object.assign({}, JSON.parse(fileContents));

		let keyAbsPath = path.resolve(this.ubpconf.wf, this.pconf.general.path, this.pconf.general.src);

		let assetsWithDetails = assetsToAdd.map((asset)=>{
			let assetRelativePath = path.relative(keyAbsPath, asset);
			return {
				"name": path.basename(asset),
				"rel": assetRelativePath
			}
		});

		const alreadyThere = (asset, list)=>{
			for(const {abs} of list){
				if(path === asset.path)
					return true
			}
			return false;
		}
		

		contentObject.keys.forEach((keyM)=>{
			if(keyM.file === keyMessage.file){
				if(!keyM.hasOwnProperty("assets")){
					keyM.assets = {};
				}
				if(!keyM.assets.hasOwnProperty("manual")){
					keyM.assets.manual = [];
				}
				assetsWithDetails.forEach((asset)=>{
					if(!alreadyThere(asset, keyM.assets.manual)){
						keyM.assets.manual.push(asset);
					}
				})
			}
		})

		return contentObject;
	}

	/**
	 * Delete an asset from a key mesasge
	 *
	 * @param      {object}  assetToDelete       The asset to delete
	 * @param      {object}  keyMessageToUpdate  The key message to update
	 * @param      {object}  fileContents        The file contents
	 * @return     {object}  { the updated content for keys file }
	 */
	deleteAsset(assetToDelete, keyMessageToUpdate, fileContents)
	{
		const contentObject = Object.assign({}, JSON.parse(fileContents));
		contentObject.keys.forEach((keyM)=>{
			if(keyM.file === keyMessageToUpdate.file){
				keyM.assets.manual.forEach((asset, index)=>{
					if(asset.name === assetToDelete.name){
						keyM.assets.manual.splice(index,1);
					}
				})
			}
		})

		return contentObject;
	}

}

/**
 * Class for automated found key assets.
 *
 * @class      KeyAssets (keymessage, projectPath, srcFolder, keyMessageType, workFolder)
 */
class KeyAssets{
	constructor(keyMessage, {path: projectPath, src, type}, {wf})
	{
		this.initialKeys = keyMessage;
		this.srcPath = path.resolve(wf, projectPath, src);
		this.projectType = type;
		this.ignoreAssets = [
			/^(#|https?:\/\/)/,
			/\.(html|pdf)$/,
			/^veeva:/,
			/^\.?\.\/shared\//
		];
		this.urlSearch = [
			/(?:href|src|data-video)=(['"])(.*?)\1/ig,
			/url\((['"]?)(.*?)\1\)/ig
		]
		this.searchForAssets = [
			/\.css$/,
		]
		this.populatedKeys = [];
		if(this.projectType === "gsk"){
			this.populateGSK();
		}else{
			this.populate();
		}

		return this.populatedKeys;
	}
	// populateGSK()
	// {
	// 	for(let key of this.initialKeys){
	// 		let keyFolder = key.file.replace("/index.html", "");
	// 		let assets = this.getGSKAssets(keyFolder);
	// 		key.assets.auto = assets;
	// 	}
	// 	this.populatedKeys = this.initialKeys;
	// }
	// getGSKAssets(keyFolder)
	// {
	// 	let results = glob.sync("**/*.*", {cwd: keyFolder, ignore: "**/*.html"});
	// 	let files = results.map(asset => {
	// 			let absPath = path.resolve(keyFolder, asset);
	// 			let relPath = path.relative(this.srcPath, absPath);
	// 			let name = path.basename(absPath);
	// 			let obj = {"name":name, "abs": absPath, "rel": relPath};
	// 			return obj;
	// 		});
	// 	return files;
	// }
	// 
	
	/**
	 * populate each key message with the found assets
	 */
	populate()
	{
		for(let key of this.initialKeys){
			let assetsInKey = this.scan(key, false);

			for(let asset of assetsInKey){
				for(let deepExp of this.searchForAssets){
					let absPath = path.resolve(this.srcPath, asset.rel);

					if(absPath.match(deepExp)){
						let assetsInCss = this.scan(asset, true);
						assetsInKey = assetsInKey.concat(assetsInCss);
					}
				}
			}
			key.assets.auto = assetsInKey;
		}
		this.populatedKeys = this.initialKeys;
	}

	/**
	 * scan the files for assets
	 *
	 * @param      {object}  key     The key
	 * @param      {boolean}  css     is css?
	 * @return     {Array}   { description_of_the_return_value }
	 */
	scan(key, css)
	{
		let keyMessage = css ? path.resolve(this.srcPath, key.rel) : path.resolve(this.srcPath, key.file);
		let keyMessagePath = path.dirname(keyMessage);

		let keyMessageContent = fs.readFileSync(keyMessage, "utf8").toString();
		for(let exp of this.urlSearch){
			let foundLinks = keyMessageContent.match(exp);
			let existingLinks = [];
			if(foundLinks){
				let purged = this.purge(foundLinks, keyMessagePath, css);
				return purged;
			}
		}
		return [];
	}
	/**
	 * only keep the assets that are real links. Check if the found assets exist
	 */
	
	purge(assetsList, keyMessagePath, css)
	{
		let returned = assetsList
			.map(asset => {
				for(let exp of this.urlSearch){
					let reg = new RegExp(exp);
					let result = reg.exec(asset);
					if(result && typeof result !== "undefined"){
						return result[2];
					}
				}
				return "not good";
			})
			.filter(asset =>{
				let absPath = path.resolve(keyMessagePath, asset);
				return asset.length && fs.existsSync(absPath) && !this.isIgnored(asset);
			})
			.map(asset => {
				let name = path.basename(asset);
				let absPath = path.resolve(keyMessagePath, asset);
				let relPath = path.relative(this.srcPath, absPath);
				let obj = {"name":name, "rel": relPath};
				return obj;
			});
	 	return returned;

	}

	/**
	 * Determines if ignored.
	 *
	 * @param      {string}   asset   The asset
	 * @return     {boolean}  True if ignored, False otherwise.
	 */
	isIgnored(asset)
	{
		for(let exp of this.ignoreAssets)
		{
			if(asset.match(exp)) return true;
		}
		return false;
	}
}

const Keys = new KeysClass();
module.exports = Keys;