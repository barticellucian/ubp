const notifier = require('node-notifier');

class Notify{
	constructor(title, message){
		notifier.notify({
			title: title,
			message: message
		}, (err, response)=>{
			// console.log(response);
		})
	}
}

module.exports = Notify;