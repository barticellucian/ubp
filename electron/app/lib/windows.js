const electron = require('electron');
const BrowserWindow = electron.BrowserWindow;
const Package = require('../package.json');
const isDev = (require('electron-is-dev') || Package.config.debug);


/// Array to hold all browser windows
// If they aren't held then they are liable to GC and to be killed
let windows = [];
module.exports.List = windows;

/// Create the initial window
module.exports.InitialWindow = function createInitialWindow() {
	let win = new BrowserWindow({
		title: 'Universal Build Process',
		width: 640,
		height: 480,
		center: true,
		resizable: false,
		fullscreenable: false,
		show: false
	});

	win.loadURL(`file://${__dirname}/../initial-screen/index.html`);

	// win.webContents.openDevTools();

	win.on('ready-to-show', () => {
		win.show();
	});

	win.on('closed', () => {
		windows.splice(windows.indexOf(win), 1);
	});

	win.on('unresponsive', () => {
		// In the real world you should display a box and do something
		console.warn('The windows is not responding');
    });

	win.webContents.on('crashed', () => {
		// In the real world you should display a box and do something
		console.error('The browser window has just crashed')
	});

	win.webContents.on('did-finish-load', () => {
		win.webContents.send('hello');
    });

	win.webContents.on('devtools-opened', () => {
		// if(isDev) win.webContents.addWorkSpace(`${__dirname}/..`);
	});

	windows.push(win);
}

module.exports.InfoWindow = function createInfoWindow(){
	let win = new electron.BrowserWindow({
		width: 600,
		height: 600,
		resizable: false
	});

	win.loadURL(`file://${__dirname}/../info.html`);
	return win;
}

module.exports.MainWindow = function createMainWindow(projectData) {
	let win = new BrowserWindow({
		title: 'Universal Build Process',
		width: 640,
		height: 480,
		center: true,
		resizable: false,
		fullscreenable: false,
		show: false
	});

	win.loadURL(`file://${__dirname}/../main-window/index.html`);


	// win.webContents.openDevTools();

	win.on('ready-to-show', () => {
		win.show();
	});

	win.on('closed', () => {
		windows.splice(windows.indexOf(win), 1);
	});

	win.on('unresponsive', () => {
		// In the real world you should display a box and do something
		console.warn('The windows is not responding');
    });

	win.webContents.on('crashed', () => {
		// In the real world you should display a box and do something
		console.error('The browser window has just crashed')
	});

	win.webContents.on('devtools-opened', () => {
		// if(isDev) win.webContents.addWorkSpace(`${__dirname}/..`);
	});

	

	windows.push(win);
	return(win);
}

module.exports.CegedimWindow = function createInteractiveWindow() {
	let win = new BrowserWindow({
		title: 'Cegedim',
		width: 640,
		height: 480,
		center: true,
		resizable: false,
		fullscreenable: false,
		show: false
	});

	win.webContents.setZoomLevelLimits(1, 1);

	win.loadURL(`file://${__dirname}/../cegedim-window/index.html`);

	win.on('ready-to-show', () => {
		win.show();
	});

	win.on('closed', () => {
		windows.splice(windows.indexOf(win), 1);
	});

	win.on('unresponsive', () => {
		// In the real world you should display a box and do something
		console.warn('The windows is not responding');
    });

	win.webContents.on('crashed', () => {
		// In the real world you should display a box and do something
		console.error('The browser window has just crashed')
	});

	win.webContents.on('did-finish-load', () => {
		win.webContents.send('hello');
    });

	win.webContents.on('devtools-opened', () => {
		// if(isDev) win.webContents.addWorkSpace(`${__dirname}/..`);
	});

	windows.push(win);
}


module.exports.IrepWindow = function createIrepWindow(keyMessages) {
	let win = new BrowserWindow({
		title: 'Universal Build Process',
		width: 480,
		height: 120,
		center: true,
		resizable: false,
		fullscreenable: false,
		show: false,
		frame: true
	});

	win.loadURL(`file://${__dirname}/../irep-window/index.html`);

	// win.webContents.openDevTools();

	win.on('ready-to-show', () => {
		win.show();
	});

	win.on('closed', () => {
		windows.splice(windows.indexOf(win), 1);
	});

	win.on('unresponsive', () => {
		// In the real world you should display a box and do something
		console.warn('The windows is not responding');
    });

	win.webContents.on('crashed', () => {
		// In the real world you should display a box and do something
		console.error('The browser window has just crashed')
	});
	
	win.webContents.on('devtools-opened', () => {
		// if(isDev) win.webContents.addWorkSpace(`${__dirname}/..`);
	});

	windows.push(win);

	return new Promise((resolve, reject)=>{
		win.webContents.on('did-finish-load', () => {
			win.webContents.send("keys", keyMessages);
			return resolve(win);
	    });		
	})
}