'use strict'

require("babel-register");
require("babel-polyfill");
const App = require("../../main-thread/App");
const expect = require('chai').expect;

describe('App wrapper', () => {
	describe('"checkIfDev"', ()=>{
		it('should return a boolean', () => {
			const isDev = App.checkIfDev();
			expect(isDev).to.be.a("boolean");
		})
	})
})