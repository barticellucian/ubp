'use strict'

require("babel-register");
require("babel-polyfill");
const Main = require("../../main-thread/Main");
const expect = require('chai').expect;

describe('Main in main-thread', () => {
	describe('"scan for projects"', ()=>{
		it('should return an array if all is ok', () => {
			expect(Main.scan("/path","/wf")).to.be.an("array");
		})
		it('should throw an error if params are undefined', () => {
			expect(Main.scan.bind(Main, undefined, undefined)).to.throw();
		})
		it('should throw an error if path or workfolder length is 0', () => {
			expect(Main.scan.bind(Main, "", "")).to.throw();
		})
	})
	describe("handleSaveDetails", ()=>{
		it('should return true if everything goes allright', ()=> {
			expect(Main.handleSaveDetails)
		})
	})
})