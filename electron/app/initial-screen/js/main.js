const { ipcRenderer } = require('electron');

class InitialScreen{
	constructor()
	{
		
		this.openBtn = document.querySelector('#open');
		this.changeWFBtn = document.querySelector('#changeWorkFolder');
		this.WFInput = document.querySelector("#workFolder");
		this.workFolder = null;

		this.getWorkFolder();
		this.getAvailableProjects();

		this.addListeners();
	}
	addListeners()
	{
		this.openBtn.addEventListener('click', (event)=>{
			ipcRenderer.send('open-app');
			//window.close();
		});
		this.changeWFBtn.addEventListener('click', (event)=>{
			ipcRenderer.send("get-path", "setWorkFolder");
			//window.close();
		});
		ipcRenderer.on("setWorkFolder", (e, wf)=>{
			this.workFolder = wf;
			this.WFInput.value = wf;
			
			this.getAvailableProjects();
		});
	}
	getWorkFolder()
	{	
		ipcRenderer.send('get-work-folder');
		ipcRenderer.on("workFolder", (e, wf)=>{
			this.workFolder = wf;
			this.WFInput.value = wf;
		});
	}
	getAvailableProjects()
	{
		ipcRenderer.send('scan-for-projects');
		ipcRenderer.on("availableProjects", (event, projects)=>{
			this.showAvailableProjects(projects);
		});
		
	}
	showAvailableProjects(projects)
	{
		let list = document.querySelector("#availableProjects");
		list.innerHTML = "";
		projects.forEach((project)=>{
			let item = document.createElement("li");
			let name = document.createElement("h3");
			name.innerHTML = project.name;
			let path = document.createElement("h5");
			let openBtn = document.createElement("button");
			openBtn.innerHTML = "Open";
			path.innerHTML = project.path;
			item.appendChild(name);
			item.appendChild(path);
			item.appendChild(openBtn);
			list.appendChild(item);

			item.addEventListener("click", (e)=>{
				ipcRenderer.send('open-app', project.path);
			});
		})
	}
}

document.addEventListener("DOMContentLoaded", ()=>{
	let initial = new InitialScreen();		
});