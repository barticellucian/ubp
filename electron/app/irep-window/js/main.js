const { ipcRenderer } = require('electron');
class ProgressWindow{
    constructor()
    {
        this.computedKey = null;
        this.keys = [];
        this.currentKey = "";
        this.currentIndex = 0;
        this.initComponent();
        this.addListeners();
    }
    initComponent()
    {
        this.computedKey = new Vue({
			el: "#computedKey",
			data: {
				currentKey: "loading...",
                currentIndex: 1,
                keysLength: 0,
                percentage: 0
			}
		});
    }
    addListeners()
    {
        ipcRenderer.on("keys", (event, keys)=>{
            this.keys = keys;
            this.computedKey.keysLength = keys.constructor === Array ? keys.length : keys.keys.length + keys.pdfs.length + (keys.shared.length > 0 ? 1 : 0);
        });

        ipcRenderer.on("keyToCompute", (event, key)=>{
            this.computedKey.currentKey = key;
		});

        ipcRenderer.on("computedKey", (event, key)=>{
            this.updateProgressBar(key);
        });
    }
    updateProgressBar()
    {
        let progressBar = document.querySelector('#p1');
        this.currentIndex++;
        this.computedKey.currentIndex = this.currentIndex + 1;
        let percentage = parseInt(this.currentIndex*100 / this.computedKey.keysLength);
        this.computedKey.percentage = percentage;
        progressBar.MaterialProgress.setProgress(percentage);
    }
}

let pw = new ProgressWindow();
module.exports = pw;