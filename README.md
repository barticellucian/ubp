START-UP
--------

1. cd electron/
2. npm i
3. npm start
4. set the work directory
5. open new/existing project
6. set project details
7. scan/build/deploy key messages

OVERVIEW
--------

Build process tool for veeva edetails. 

The app is build on Electron and incorporates server and client sides. They are communicating to each other through an event based system that uses [ipc main](https://github.com/mozilla/positron-electron/blob/master/docs/api/ipc-main.md).

We are using es6 for writing the code and features like async/await which are not supported yet, so that's why we have to use [babel](https://babeljs.io/) as well.

On the client side we are using [vue.js](https://vuejs.org/) and babel again.

All the errors are saved in the logs file per session. That means the logs for the previous session will not exist in the actual one.

We use using [node-notifier](https://www.npmjs.com/package/node-notifier) to send notifications when important stuff is happening.

Although not a lot of tests are written yet, the testing process is there and you can start it with "npm test". We use mohca([electron-mocha](https://github.com/jprichardson/electron-mocha)) and [chai](http://chaijs.com/) for assertions.



SERVER
------

![server data flow](http://172.18.122.10/hivegroup/universal_build_process/raw/master/server.png "Server data flow")


#### 1(main.js)
It's main purpose is to wrap all the content within the the babel polyfills.
This is required so we can use async await

```javascript
require("babel-register");
require("babel-polyfill");
require("./main-thread/App");
```

---

#### 2(app.js)

---

#### 3(/main-thread/App.js)

Initiates the functionality for the app and handles the main electron events and startup actions

---

#### 4(Main.js)

Handles the General Tab and Ftp related events and also has the methods that deal with the irep and deploy.

Extends the Base Class (Base.js -> point 7)

some functionality:

```javascript
ipc.on("scan-for-projects", (event)=>{...});
ipc.on("save-details", (event)=>{...});
ipc.on("get-details", (event)=>{...});
ipc.on("irep-build", (event)=>{...});
ipc.on("ftp-deploy", (event)=>{...});
ipc.on("build-key", (event)=>{...});
ipc.on("deploy-key", (event)=>{...});
ipc.on("get-work-folder", (event)=>{...});
ipc.on("copy-to-clipboard", (event)=>{...});
```

---

#### 5(Keys.js)

Class that handles the events and operations for the Key Messages.

Extends the Base Class (Base.js -> point 7)

some functionality:

```javascript
ipc.on("scan-keys", (event)=>{...});
ipc.on("add-specific-assets", (event)=>{...});
ipc.on("delete-specific-assets", (event)=>{...});
ipc.on("update-key-data", (event)=>{...});
```

---

#### 6(Logs.js)

Class that handles the events and operations for the Logs.

Extends the Base Class (Base.js -> point 7)

new logs / get logs

---

#### 7(Base.js)

Parent Class for Main/Keys/Logs.

It works as a common interface between the classes and ubp configs / project configs

Has methods that read or operate changes on the config databases

---

#### 8(IrepLoader.js)

Wraps the newly forked Irep process in the babel pollyfil so we can use async/await.

---

#### 9(Irep.js)

The code for the Irep process.

creates keys folders -> copy keys -> copy assets for those keys -> take screenshots -> zips them up and creates ctl files

For the key messages that are based on a .html file we also inject some code in the .html file. That will give indications on
how to compute the key messages names.

---

#### 10(FtpDeploy.js)

1. parse the key messages
2. connects to ftp
3. creates the folders
4. copy files over

---

#### 11(irep/screenshot.js)

Handles the screenshooting of the key messages. 

We are using phantom / phantom js and graphic magic.

---

#### 12(/db/ftp-thg-sandbox.json)

Here's where we are saving the default ftp defails for the THG sandbox.

---

#### 13(/models/config.js)

Handles get/set ubp configuration details.

---

#### 14(/models/projectConfig.js)

Handles get/set ubp project configuration details.

---

#### 15([appData]/UBP/config.json)

Details like the "work folder" are saved here. These are general UBP configuration details.

---



CLIENT
------

![client data flow](http://172.18.122.10/hivegroup/universal_build_process/raw/master/client.png "Client data flow")

#### 1(InitialWindow)

This is the first window you see. There's an "Open Project button", a "Select work folder" button (which is essential btw) and a list of projects in the 
work folder you selected.

---

#### 2(MainWindow)

The main window is the window that you see when you open a project. There's the main functionality interface. You will find four tabs there General/Ftp/Keys and Logs.

Some of the things in there will be autocompleted from database files or from the selections you made already, but the user needs to complete the details in the first tab at least.



---

#### 3(IrepWindow)

The Irep window only appears when you irep/ deploy key messages. It shows the progress of the task and the current processed key message name

---

#### 4(InitialWindow/main.js)

Holds the functionality for the initial Window.

---

#### 5(MainWindow/main.js)

Initiates the classes for the 4 tabs and the tabs functionality.

---

#### 6(IrepWindow/main.js)

The functionality for the IrepWindow

---

#### 7(Tab.js)

The base class from which the tab classes are extended.

Initiates the main details arrays/objects, adds vue and handles generic listeners.

---

#### 8(TabGeneral)

Extends Tab.js.

Handles this tab's specific listeners and intiates the vue functionality for it.

---

#### 9(TabFtp)

Extends Tab.js.

Handles this tab's specific listeners and intiates the vue functionality for it.

---

#### 10(TabKeys)

Extends Tab.js.

Handles this tab's specific listeners and intiates the vue functionality for it.

---

#### 11(TabLogs)

Extends Tab.js.

Handles this tab's specific listeners and intiates the vue functionality for it.

---
